ZO_CreateStringId('SI_AUTO_MAIL_TITLE', 'AutoMail')

ZO_CreateStringId('AUTO_MAIL_RECEIPT_EXPIRED_ITEM', '<<1>> listing expired')

ZO_CreateStringId('AUTO_MAIL_SETTING_AUTOMATIC', 'Automatic retrieval')
ZO_CreateStringId('AUTO_MAIL_SETTING_AUTOMATIC_TIP', 'Automatically retrieve attached items and gold as mail is opened')
ZO_CreateStringId('AUTO_MAIL_SETTING_PRINT_EXPIRED', 'Print expired items')
ZO_CreateStringId('AUTO_MAIL_SETTING_PRINT_EXPIRED_TIP', 'Print details for items that expire from the guild store')
ZO_CreateStringId('AUTO_MAIL_SETTING_DELETE_AFTER', 'Delete mail')
ZO_CreateStringId('AUTO_MAIL_SETTING_DELETE_AFTER_TIP', 'Delete mail after taking all attached items and gold')
