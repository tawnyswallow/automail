AutoMail = {
    name = 'AutoMail',
    savedVarsVersion = 1,
    defaults = {
        Automatic = false,
        PrintExpiredGuildItems = true,
        DeleteMail = true
    }
}

local inboxButton = {
    {
        name = GetString(SI_AUTO_MAIL_TITLE),
        keybind = 'AUTO_MAIL_PROCESS_CURRENT_MAIL',
        control = self,
        callback = function(...) self:ProcessCurrentMail(...) end
    },
    alignment = KEYBIND_STRIP_ALIGN_LEFT
}

function AutoMail:Initialize()
    -- Variable initialization
    self.settings = ZO_SavedVars:NewAccountWide('AutoMail_Settings', self.savedVarsVersion, nil, self.defaults)

    -- Menu initialization
    local LAM = LibStub('LibAddonMenu-2.0')
    local panelData = {
        type = 'panel',
        name = GetString(SI_AUTO_MAIL_TITLE)
    }
    LAM:RegisterAddonPanel('AutoMailOptions', panelData)

    local optionsData = {
        [1] = {
            type = 'checkbox',
            name = GetString(AUTO_MAIL_SETTING_AUTOMATIC),
            tooltip = GetString(AUTO_MAIL_SETTING_AUTOMATIC_TIP),
            getFunc = function() return AutoMail.settings.Automatic end,
            setFunc = function(value) AutoMail.settings.Automatic = value end
        },
        [2] = {
            type = 'checkbox',
            name = GetString(AUTO_MAIL_SETTING_PRINT_EXPIRED),
            tooltip = GetString(AUTO_MAIL_SETTING_PRINT_EXPIRED_TIP),
            getFunc = function() return AutoMail.settings.PrintExpiredGuildItems end,
            setFunc = function(value) AutoMail.settings.PrintExpiredGuildItems = value end
        },
        [3] = {
            type = 'checkbox',
            name = GetString(AUTO_MAIL_SETTING_DELETE_AFTER),
            tooltip = GetString(AUTO_MAIL_SETTING_DELETE_AFTER_TIP),
            getFunc = function() return AutoMail.settings.DeleteMail end,
            setFunc = function(value) AutoMail.settings.DeleteMail = value end
        }
    }
    LAM:RegisterOptionControls('AutoMailOptions', optionsData)

    -- Keybinding initialization
    ZO_CreateStringId('SI_BINDING_NAME_AUTO_MAIL_PROCESS_CURRENT_MAIL', GetString(SI_AUTO_MAIL_TITLE))

    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_MAIL_OPEN_MAILBOX, function(...) KEYBIND_STRIP:AddKeybindButtonGroup(inboxButton) end)
    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_MAIL_CLOSE_MAILBOX, function(...) KEYBIND_STRIP:RemoveKeybindButtonGroup(inboxButton) end)

    -- Mail hooks
    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_MAIL_READABLE, function(_, mailId) self:OnMailReadable(mailId) end)
end

function AutoMail:OnAddonLoaded(event, addonName)
    if addonName == self.name then
        self:Initialize()
		EVENT_MANAGER:UnregisterForEvent(self.name, EVENT_ADD_ON_LOADED)
    end
end

function AutoMail:OnMailReadable(mailId)
    if self.settings.Automatic then
        self:ProcessCurrentMail()
    end
end

function AutoMail:Print(message, ...)
    CHAT_SYSTEM:AddMessage(zo_strformat(GetString(message), ...))
end

function AutoMail:ProcessCurrentMail()
    local mailId = MAIL_INBOX:GetOpenMailId()

    if mailId then
        local numAttachments, attachedGold, codAmount = GetMailAttachmentInfo(mailId)

        if numAttachments == 0 and attachedGold == 0 then
            local nextMailId = GetNextMailId(mailId)

            if nextMailId then
                MAIL_INBOX:RequestReadMessage(nextMailId)
            end
        else
            MAIL_INBOX:TryTakeAll()

            if self.settings.PrintExpiredGuildItems then
                local senderDisplayName, senderCharacterName, subject = GetMailItemInfo(mailId)
                
                if subject == 'Item Expired' then
                    self:Print(AUTO_MAIL_RECEIPT_EXPIRED_ITEM, GetAttachedItemLink(mailId, 1, LINK_STYLE_BRACKETS))
                end
            end

            if self.settings.DeleteMail then
                zo_callLater(function() MAIL_INBOX:ConfirmDelete(mailId) end, 250)
            end
        end
    end
end

--[[
function AutoMail:ProcessMail()
    RequestOpenMailbox()
    local numMails = GetNumMailItems()

    if numMails == 0 then
        CloseMailbox()
        return
    end

    self:Print(AUTO_MAIL_PROCESSING, numMails)

    local mailId = nil
    local totalItems, totalGold = 0, 0

    for i = 1, GetNumMailItems(), 1 do
        mailId = GetNextMailId(mailId)
        
        if mailId ~= nil then
            local unread, returned, fromSystem = GetMailFlags(mailId)
            local numAttachments, attachedGold, codAmount = GetMailAttachmentInfo(mailId)

            if codAmount > 0 and codAmount > GetCurrencyAmount(CURT_MONEY, CURRENCY_LOCATION_CHARACTER) then
                self:Print(AUTO_MAIL_RECEIPT_NOT_ENOUGH_COD, codAmount)
            else
                if numAttachments > 0 then
                    totalItems = totalItems + numAttachments
                    TakeMailAttachedItems(mailId)

                    if self.settings.PrintExpiredGuildItems then
                        local senderDisplayName, senderCharacterName, subject = GetMailItemInfo(mailId)
                        
                        if subject == 'Item Expired' then
                            self:Print(AUTO_MAIL_RECEIPT_EXPIRED_ITEM, GetAttachedItemLink(mailId, 1, LINK_STYLE_BRACKETS))
                        end
                    end
                end

                if attachedGold > 0 then
                    totalGold = totalGold + attachedGold
                    TakeMailAttachedMoney(mailId)
                end

                if fromSystem and self.settings.DeleteMail then
                    DeleteMail(mailId, true)
                end
            end
        end
    end

    if self.settings.PrintSummary then
        self:Print(AUTO_MAIL_RECEIPT_SUMMARY, totalItems, totalGold)
    end

    CloseMailbox()
end
]]

EVENT_MANAGER:RegisterForEvent(AutoMail.name, EVENT_ADD_ON_LOADED, function(...) AutoMail:OnAddonLoaded(...) end)
